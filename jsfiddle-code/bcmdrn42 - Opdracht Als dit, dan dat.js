const person = new Person();

const beardColor = "orange";
const hairColor = "yellow";
const speakingText = "Mijn baardkleur is belangrijk!";

person.setBeardColor(beardColor);
person.setHairColor(hairColor);

person.setText(speakingText);