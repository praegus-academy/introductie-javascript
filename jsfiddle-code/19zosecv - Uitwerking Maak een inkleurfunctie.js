const person = new Person();

function spreek(text) {
  person.setSpeakingLightOn();
  person.setText(text);
}
spreek("Als je haar maar goed zit!");

// Gebruik de onderstaande functies om de kleuren te zetten
// Vergeet niet de kleur nog mee te geven als parameter:

function kleurIn(kleurNaam) {
  person.setBeardColor(kleurNaam);
  person.setHairColor(kleurNaam);
  person.setEyebrowsColor(kleurNaam);
}

kleurIn("gold");
