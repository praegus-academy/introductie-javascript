const person = new Person();

let speakingText = "5 + 5 = " + 5 + 5; // 5 + 5 = 55

speakingText = "5 + 5 = " + (5 + 5); // 5 + 5 = 10

speakingText = "5 * 5 = " + 5 * 5; // 5 * 5 = 25

speakingText = "5 - 5 = " + 5 - 5; // NaN

speakingText = "5 - 5 = " + (5 - 5); // 5 - 5 = 0

speakingText = "5 / 5 = " + 5 / 5; // 5 / 5 = 1

speakingText = "5 + 5 = " + (Number("5") + Number("5")); // 5 + 5 = 10

person.setText(speakingText);