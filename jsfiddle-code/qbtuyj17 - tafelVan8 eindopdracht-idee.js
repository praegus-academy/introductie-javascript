const person = new Person();
const beardColor = "orange";
person.setBeardColor(beardColor);
const ENTER = "\n";

function spreek(text) {
  person.setSpeakingLightOn();
  person.setText(text);
}

function vermenigvuldig(a, b) {
  const uitkomst = a * b;
  const somText = "" + a + " x " + b + " = " + uitkomst;
  return somText;
}

function tafelVan(a) {
  function maakTafel(b) {
    const somText = vermenigvuldig(a, b);
    return somText;
  }

  const lijst = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const lijstMetSommen = lijst.map(maakTafel);
  return lijstMetSommen;
}

const tafelVan8 = tafelVan(8);
spreek(tafelVan8.join("\n"));


const tafelVan9 = tafelVan(9);
spreek(tafelVan9.join("\n"));
