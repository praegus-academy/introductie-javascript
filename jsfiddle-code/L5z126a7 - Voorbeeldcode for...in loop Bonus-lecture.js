const person = new Person();
const beardColor = "orange";
const ENTER = "\n";

function spreek(text) {
  person.setSpeakingLightOn();
  person.setText(text);
}

function gebruikObject(config) {
  person.setBeardColor(config.beardColor);
  person.setHairColor(config.hairColor);
}

let mijnConfig = {
  beardColor: "gold",
  hairColor: "brown",
  age: 36
};

for(const prop in mijnConfig) {
  spreek("Property met naam " + prop + " heeft de waarde " + mijnConfig[prop]);
}
gebruikObject(mijnConfig);
