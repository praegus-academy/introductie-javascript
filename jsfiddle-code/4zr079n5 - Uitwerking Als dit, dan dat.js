const person = new Person();

const beardColor = "orange";
const hairColor = "orange";
const speakingText = "Mijn baardkleur is belangrijk!";

person.setBeardColor(beardColor);
person.setHairColor(hairColor);

if (beardColor === hairColor) {
  person.setText(speakingText);
}
