const person = new Person();
const ENTER = "\n";

const boodschappenlijstje = ["melk", "kaas", "ham", "chocolade", "thee"];
let titel = "Mijn boodschappenlijstje: " + ENTER;


function opsomming(text) {
  return " - " + text;
}
const mooierLijstje = boodschappenlijstje.map(opsomming);

const text = titel + mooierLijstje.join(ENTER);
person.setText(text);