const person = new Person();

const beardColor = "orange";
const speakingText = "Ik leer JavaScript!";

person.setBeardColor(beardColor);
person.setText(speakingText);

// person.setDefaultStyles(); // Set back the default styles

// person.setSkinColor("red"); // Set the skin color, value can be a named color, hex or RGB
// person.setEyesColor("red"); // Set the eyes color, value can be a named color, hex or RGB
// person.setEyesOuterColor("red"); // Set the eyes outer color, value can be a named color, hex or RGB
// person.setHairColor("red"); // Set the hair color, value can be a named color, hex or RGB
// person.setEyebrowsColor("red"); // Set the eyebrowns color, value can be a named color, hex or RGB
// person.setBeardColor("red"); // Set the beard color, value can be a named color, hex or RGB
// person.setShirtColor("red"); // Set the shirt color, value can be a named color, hex or RGB
// person.setJacketColor("red"); // Set the jacket color, value can be a named color, hex or RGB
// person.setSpeakingTableColor("red"); // Set the speaking table color, value can be a named color, hex or RGB

// person.setSpeakingLightOn(); // Set the desk speaking light to green
// person.setSpeakingLightOff(); // Set the desk speaking light to red


// person.setText("Hello World!"); // Create a new text bubble, up to 4
// person.clearText(); // Make all the text bubbles hidden
// person.getTextBubbleContent(); // Return an array of all the contents of the text bubbles

// person.getProperties(); // Return an array with object of properties that has been set to the charachter, it also includes value set function for that specific property
