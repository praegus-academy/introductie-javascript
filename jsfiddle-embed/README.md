
## How to use it

Add the following script tag inside the html body:

    `<script async src="//praegus.nl/jsfiddle-embed.js" data-slug="JSFIDDLE_ID"></script>`

Change the data-slug value to de JSFiddle ID.

It is also possible to specify the available tabs in the embedded JSFiddle:

    `<script async src="//praegus.nl/jsfiddle-embed.js" data-slug="JSFIDDLE_ID" data-tabs="js,html,css,result"></script>`
